$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/buyticket.feature");
formatter.feature({
  "name": "Buying the ticket",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Timetable searach returns correct routes",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "\u003csource\u003e and \u003cdestination\u003e are entered",
  "keyword": "When "
});
formatter.step({
  "name": "the search for timetables is pressed",
  "keyword": "And "
});
formatter.step({
  "name": "route selection page opens with routes from \u003csource\u003e to \u003cdestination\u003e",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "source",
        "destination"
      ]
    },
    {
      "cells": [
        "\"Turku\"",
        "\"Helsinki\""
      ]
    },
    {
      "cells": [
        "\"Helsinki\"",
        "\"Porvoo\""
      ]
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "The user will load the matkahuolto site",
  "keyword": "Given "
});
formatter.match({
  "location": "cucuJava.the_user_will_load_the_matkahuolto_site()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Timetable searach returns correct routes",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "\"Turku\" and \"Helsinki\" are entered",
  "keyword": "When "
});
formatter.match({
  "location": "cucuJava.turku_and_Helsinki_are_entered(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the search for timetables is pressed",
  "keyword": "And "
});
formatter.match({
  "location": "cucuJava.the_search_for_timetables_is_pressed()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "route selection page opens with routes from \"Turku\" to \"Helsinki\"",
  "keyword": "Then "
});
formatter.match({
  "location": "cucuJava.route_selection_page_opens_with_routes_from_Turku_to_Helsinki(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "The user will load the matkahuolto site",
  "keyword": "Given "
});
formatter.match({
  "location": "cucuJava.the_user_will_load_the_matkahuolto_site()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Timetable searach returns correct routes",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "\"Helsinki\" and \"Porvoo\" are entered",
  "keyword": "When "
});
formatter.match({
  "location": "cucuJava.turku_and_Helsinki_are_entered(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the search for timetables is pressed",
  "keyword": "And "
});
formatter.match({
  "location": "cucuJava.the_search_for_timetables_is_pressed()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "route selection page opens with routes from \"Helsinki\" to \"Porvoo\"",
  "keyword": "Then "
});
formatter.match({
  "location": "cucuJava.route_selection_page_opens_with_routes_from_Turku_to_Helsinki(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Onnibus tickets are not available",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "\u003csource\u003e and \u003cdestination\u003e is entered",
  "keyword": "When "
});
formatter.step({
  "name": "the search for timetables are pressed",
  "keyword": "And "
});
formatter.step({
  "name": "connections operated with Onnibus do not have associated Buy button",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "source",
        "destination"
      ]
    },
    {
      "cells": [
        "\"Turku\"",
        "\"Helsinki\""
      ]
    },
    {
      "cells": [
        "\"Helsinki\"",
        "\"Porvoo\""
      ]
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "The user will load the matkahuolto site",
  "keyword": "Given "
});
formatter.match({
  "location": "cucuJava.the_user_will_load_the_matkahuolto_site()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Onnibus tickets are not available",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "\"Turku\" and \"Helsinki\" is entered",
  "keyword": "When "
});
formatter.match({
  "location": "cucuJava.turku_and_Helsinki_is_entered(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the search for timetables are pressed",
  "keyword": "And "
});
formatter.match({
  "location": "cucuJava.the_search_for_timetables_are_pressed()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "connections operated with Onnibus do not have associated Buy button",
  "keyword": "Then "
});
formatter.match({
  "location": "cucuJava.connections_operated_with_Onnibus_do_not_have_associated_Buy_button()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "The user will load the matkahuolto site",
  "keyword": "Given "
});
formatter.match({
  "location": "cucuJava.the_user_will_load_the_matkahuolto_site()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Onnibus tickets are not available",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "\"Helsinki\" and \"Porvoo\" is entered",
  "keyword": "When "
});
formatter.match({
  "location": "cucuJava.turku_and_Helsinki_is_entered(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the search for timetables are pressed",
  "keyword": "And "
});
formatter.match({
  "location": "cucuJava.the_search_for_timetables_are_pressed()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "connections operated with Onnibus do not have associated Buy button",
  "keyword": "Then "
});
formatter.match({
  "location": "cucuJava.connections_operated_with_Onnibus_do_not_have_associated_Buy_button()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("src/test/resources/mainpage.feature");
formatter.feature({
  "name": "Main page essentials",
  "description": "",
  "keyword": "Feature"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "The user loads the Matkahuolto web site",
  "keyword": "Given "
});
formatter.match({
  "location": "cucuJava.the_user_loads_the_Matkahuolto_web_site()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Test if logo is on the main page",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The logo is visible",
  "keyword": "Then "
});
formatter.match({
  "location": "cucuJava.the_logo_is_visible()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("src/test/resources/parcel.feature");
formatter.feature({
  "name": "Parcel tracking",
  "description": "",
  "keyword": "Feature"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "The user has loaded the Matkahuolto web site",
  "keyword": "Given "
});
formatter.match({
  "location": "cucuJava.the_user_has_loaded_the_Matkahuolto_web_site()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Consignment numbered is not correct",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user enters an incorrect consignment number",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "The user should get a notification",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "The user does not see information",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "The user has loaded the Matkahuolto web site",
  "keyword": "Given "
});
formatter.match({
  "location": "cucuJava.the_user_has_loaded_the_Matkahuolto_web_site()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Consignment numbered exists",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user enters the consignment number",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "The consignment number exists",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "The user should see matching information",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "A written number is sought",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "The user enters \u003cnumber\u003e",
  "keyword": "When "
});
formatter.step({
  "name": "\u003csent_date\u003e",
  "keyword": "Then "
});
formatter.step({
  "name": "\u003carrival_date\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "\u003corigin\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "\u003cdestination\u003e are visible",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "number",
        "sent_date",
        "arrival_date",
        "origin",
        "destination"
      ]
    },
    {
      "cells": [
        "00370716489535264169",
        "22.3.2017",
        "24.3.2017",
        "Helsinki",
        "Littoinen"
      ]
    },
    {
      "cells": [
        "MH470825031FI",
        "23.3.2018",
        "27.3.2018",
        "Savonlinna",
        "Turku"
      ]
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "The user has loaded the Matkahuolto web site",
  "keyword": "Given "
});
formatter.match({
  "location": "cucuJava.the_user_has_loaded_the_Matkahuolto_web_site()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "A written number is sought",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "The user enters 00370716489535264169",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "22.3.2017",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "24.3.2017",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Helsinki",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Littoinen are visible",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "The user has loaded the Matkahuolto web site",
  "keyword": "Given "
});
formatter.match({
  "location": "cucuJava.the_user_has_loaded_the_Matkahuolto_web_site()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "A written number is sought",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "The user enters MH470825031FI",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "23.3.2018",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "27.3.2018",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Savonlinna",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Turku are visible",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "passed"
});
formatter.uri("src/test/resources/ticketsales.feature");
formatter.feature({
  "name": "Ticket Sales",
  "description": "",
  "keyword": "Feature"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "The user loadededed the Matkahuolto web site",
  "keyword": "Given "
});
formatter.match({
  "location": "cucuJava.the_user_loadededed_the_Matkahuolto_web_site()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Ticket sales link works",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "ticket sales link is pressed",
  "keyword": "When "
});
formatter.match({
  "location": "cucuJava.ticket_sales_link_is_pressed()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The online ticket sales page opens",
  "keyword": "Then "
});
formatter.match({
  "location": "cucuJava.the_online_ticket_sales_page_opens()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});