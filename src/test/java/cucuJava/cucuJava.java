package cucuJava; 

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By; 
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ById;
//import org.openqa.selenium.firefox.FirefoxDriver; 
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.After;
import cucumber.api.java.en.Given; 
import cucumber.api.java.en.Then; 
import cucumber.api.java.en.When; 

public class cucuJava {
   WebDriver driver = null; 
    
   // Close chrome after each scenario
   @After
   public void closeChrome(){
       driver.close();
   }

   // Method to open Matkahuolto main page
    private void openMatkahuoltoSite() {
        System.setProperty("webdriver.chrome.driver", "C:\\ChromeDriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.navigate().to("https://www.matkahuolto.fi");
    }

    @Given("^The user will load the matkahuolto site$")
    public void the_user_will_load_the_matkahuolto_site() {
        openMatkahuoltoSite();
    }

    @When("^\"([^\"]*)\" and \"([^\"]*)\" are entered$")
    public void turku_and_Helsinki_are_entered(String arg1, String arg2) {
        WebElement departureInput = driver.findElement(By.name("departurePlaceName"));
        WebElement arrivalInput = driver.findElement(By.name("arrivalPlaceName"));
        departureInput.sendKeys(arg1);
        arrivalInput.sendKeys(arg2);
        assertEquals(departureInput.getAttribute("value"),arg1);
        assertEquals(arrivalInput.getAttribute("value"),arg2);
    }

    @When("^the search for timetables is pressed$")
    public void the_search_for_timetables_is_pressed() {
        driver.findElement(By.xpath("//form[@action='/reittihaku/search']//button")).click();
        assertTrue(driver.findElement(By.id("connectionLists")).isEnabled());
    }

    @Then("^route selection page opens with routes from \"([^\"]*)\" to \"([^\"]*)\"$")
    public void route_selection_page_opens_with_routes_from_Turku_to_Helsinki(String arg1, String arg2) {
        List<WebElement> routeList = driver.findElements(By.className("connectionListRowWrapper"));
        for (WebElement element : routeList){
            assertTrue(element.findElement(By.className("departureColumn")).findElement(By.className("placeTitle")).getText().contains(arg1));
            assertTrue(element.findElement(By.className("arrivalColumn")).findElement(By.className("placeTitle")).getText().contains(arg2));
        }
            
    }

    @When("^\"([^\"]*)\" and \"([^\"]*)\" is entered$")
    public void turku_and_Helsinki_is_entered(String arg1, String arg2) {
        WebElement departureInput = driver.findElement(By.name("departurePlaceName"));
        WebElement arrivalInput = driver.findElement(By.name("arrivalPlaceName"));
        departureInput.sendKeys(arg1);
        arrivalInput.sendKeys(arg2);
        assertEquals(departureInput.getAttribute("value"), arg1);
        assertEquals(arrivalInput.getAttribute("value"), arg2);
    }

    @When("^the search for timetables are pressed$")
    public void the_search_for_timetables_are_pressed() {
        driver.findElement(By.xpath("//form[@action='/reittihaku/search']//button")).click();
        assertTrue(driver.findElement(By.id("connectionLists")).isEnabled());
    }

    @Then("^connections operated with Onnibus do not have associated Buy button$")
    public void connections_operated_with_Onnibus_do_not_have_associated_Buy_button() {
        List<WebElement> routeList = driver.findElements(By.className("connectionListRowWrapper"));
        for (WebElement element : routeList){
            if(element.findElement(By.className("companyColumn")).getText().contains("Onnibus")){
                assertFalse(element.findElement(By.name("selectConnection")).isDisplayed());
            }
        }
    }

    @Given("^The user loads the Matkahuolto web site$")
    public void the_user_loads_the_Matkahuolto_web_site() {
        openMatkahuoltoSite();
    }

    @Then("^The logo is visible$")
    public void the_logo_is_visible() {
        assertTrue(driver.findElement(By.id("logo")).isDisplayed());
    }

    @Given("^The user has loaded the Matkahuolto web site$")
    public void the_user_has_loaded_the_Matkahuolto_web_site() {
        openMatkahuoltoSite();
    }

//     @When("^The user enters an incorrect consignment number$")
//     public void the_user_enters_an_incorrect_consignment_number() {
//         // Write code here that turns the phrase above into concrete actions
//         throw new PendingException();
//     }

//     @Then("^The user should get a notification$")
//     public void the_user_should_get_a_notification() {
//         // Write code here that turns the phrase above into concrete actions
//         throw new PendingException();
//     }

//     @Then("^The user does not see information$")
//     public void the_user_does_not_see_information() {
//         // Write code here that turns the phrase above into concrete actions
//         throw new PendingException();
//     }

//     @When("^The user enters the consignment number$")
//     public void the_user_enters_the_consignment_number() {
//         // Write code here that turns the phrase above into concrete actions
//         throw new PendingException();
//     }

//     @When("^The consignment number exists$")
//     public void the_consignment_number_exists() {
//         // Write code here that turns the phrase above into concrete actions
//         throw new PendingException();
//     }

//     @Then("^The user should see matching information$")
//     public void the_user_should_see_matching_information() {
//         // Write code here that turns the phrase above into concrete actions
//         throw new PendingException();
//     }

//     @When("^The user enters (\\d+)$")
//     public void the_user_enters(int arg1) {
//         // Write code here that turns the phrase above into concrete actions
//         throw new PendingException();
//     }

// @Then("^(\\d+)\\.(\\d+)\\.(\\d+)$")
// public void (int arg1,
//     int arg2,
//     int arg3)
//     {
//         // Write code here that turns the phrase above into concrete actions
//         throw new PendingException();
//     }

//     @Then("^Helsinki$")
//     public void helsinki() {
//         // Write code here that turns the phrase above into concrete actions
//         throw new PendingException();
//     }

//     @Then("^Littoinen are visible$")
//     public void littoinen_are_visible() {
//         // Write code here that turns the phrase above into concrete actions
//         throw new PendingException();
//     }

//     @When("^The user enters MH(\\d+)FI$")
//     public void the_user_enters_MH_FI(int arg1) {
//         // Write code here that turns the phrase above into concrete actions
//         throw new PendingException();
//     }

//     @Then("^Savonlinna$")
//     public void savonlinna() {
//         // Write code here that turns the phrase above into concrete actions
//         throw new PendingException();
//     }

//     @Then("^Turku are visible$")
//     public void turku_are_visible() {
//         // Write code here that turns the phrase above into concrete actions
//         throw new PendingException();
//     }

    @Given("^The user loadededed the Matkahuolto web site$")
    public void the_user_loadededed_the_Matkahuolto_web_site() {
        openMatkahuoltoSite();
    }

    @When("^ticket sales link is pressed$")
    public void ticket_sales_link_is_pressed() {
        driver.findElement(By.className("discounts")).findElements(By.tagName("a")).get(0).click();
    }

    @Then("^The online ticket sales page opens$")
    public void the_online_ticket_sales_page_opens() {
        assertTrue(driver.findElement(By.className("departure")).isEnabled());
    }

}