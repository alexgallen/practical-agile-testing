#tämä dojossa jos aikaa
Feature: Parcel tracking
    Background:
        Given The user has loaded the Matkahuolto web site

    Scenario: Consignment numbered is not correct
        When The user enters an incorrect consignment number
        Then The user should get a notification
        And The user does not see information

    Scenario: Consignment numbered exists
        When The user enters the consignment number
        And The consignment number exists
        Then The user should see matching information

    Scenario Outline: A written number is sought
        When The user enters <number>
        Then <sent_date>
        And <arrival_date>
        And <origin>
        And <destination> are visible

        Examples:
            | number               | sent_date | arrival_date | origin     | destination |
            | 00370716489535264169 | 22.3.2017 | 24.3.2017    | Helsinki   | Littoinen   |
            | MH470825031FI        | 23.3.2018 | 27.3.2018    | Savonlinna | Turku       |