Feature: Buying the ticket
    Background:
        Given The user will load the matkahuolto site

    Scenario Outline: Timetable searach returns correct routes
        When <source> and <destination> are entered
        And the search for timetables is pressed
        Then route selection page opens with routes from <source> to <destination>

        Examples:
            | source | destination |
            | "Turku"  | "Helsinki"    |
            | "Helsinki" | "Porvoo" |

    Scenario Outline: Onnibus tickets are not available
        When <source> and <destination> is entered
        And the search for timetables are pressed
        Then connections operated with Onnibus do not have associated Buy button

        Examples:
            | source | destination |
            | "Turku"  | "Helsinki"    |
            | "Helsinki" | "Porvoo" |